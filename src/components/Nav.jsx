import React, { Component } from 'react';

export default class Nav extends Component {
    change(e) {
        this.props.changeAi(parseInt(e.target.value))
    }

    toggleMute(e) {
        this.props.toggleMute()
    }

    render() {
        return (
            <div className="container-fluid navBar">
                <div className="row no-padding">
                    <div className="col-6">
                        <i className={`fa ${ this.props.muteState ? 'fa-volume-off' : 'fa-volume-up' } muteSounds`} onClick={event => this.toggleMute()}></i>
                    </div>
                    <div className="col-6 text-right">
                        AI Raskusaste:
                        <select onChange={value => this.change(value)} value={this.props.aiDiff}>
                            <option value="0">Loll</option>
                            <option value="1">Impossible</option>
                        </select>
                    </div>
                </div>
            </div>
        );
    }
}