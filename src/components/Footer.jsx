import React, { Component } from 'react';
import playerImage from '../assets/img/player.jpg';
import aiImage from '../assets/img/ai.jpg';

export default class Footer extends Component {

    render() {
        return (
            <div className="container-fluid">
                <div className="row footer">
                    <div className={`col-4 col-player ${ (this.props.currentTurn === 'X' && !this.props.gameOver) || this.props.winner === 'Player' ? 'itsTurn' : '' }`}>
                        <div className="avatar player">
                            <img src={playerImage} alt="PLAYER" />                            
                        </div>
                        <div className="playerInfo d-none d-md-block d-lg-block">Player - (X)</div> 
                    </div>
                    <div className="col-4 col-mid">
                       {this.props.playerScore} - <span className="d-none d-md-block d-lg-block">[{this.props.tieCount}] -</span> {this.props.aiScore}
                    </div>
                    <div className={`col-4 col-ai ${ (this.props.currentTurn === 'O' && !this.props.gameOver) || this.props.winner === 'AI' ? 'itsTurn' : '' }`}>
                        <div className="aiInfo d-none d-md-block d-lg-block">(O) -AI</div>
                        <div className="avatar ai">                            
                            <img src={aiImage} alt="AI" />                            
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
