import React, { Component } from 'react';
import './App.css';
import Nav from './components/Nav';
import Footer from './components/Footer';

/* Helid */
var fight = new Audio('/assets/audio/fight.wav');
var player_move = new Audio('/assets/audio/player_move.wav');
var ai_move = new Audio('/assets/audio/ai_move.wav');
var shinak_wins = new Audio('/assets/audio/shinak_wins.wav');
var quan_chi_wins = new Audio('/assets/audio/quan_chi_wins.wav');
var tie = new Audio('/assets/audio/tie.wav');

class App extends Component {
  
  constructor(props) {
    super(props)
    this.state = {
      P1_SYMBOL: "X",
      AI_SYMBOL: "O",
      currentTurn: "X",
      board: [
        "","","","","","","","",""
      ],      
      movesLeft: 9,
      winner: undefined,
      gameOver: false,
      aiScore: 0,
      playerScore: 0,
      aiDifficulty: 1,
      tieCount: 0,  
      winningCombo: [],      
      audioMuted  : false    
    }
  }

  /* Heli vaigistamine */
  toggleMute() {
    const muteState = (this.state.audioMuted) ? false : true;
    this.setState({ audioMuted: muteState })
  }

  /* Muuda Arvuti Raskusastet */
  updateAiDifficulty(diff) {
    this.setState({ aiDifficulty: diff })
  }
 
  /* Leia kõik tühjad väljad laual */
  boardFreeSpots(board) {
    let result = [];    
    for(let i=0; i < board.length; i++) {
      if(!board[i]) { result.push(i) };      
    }
    return result;
  }

  /* Proovi läbi kõik arvuti käigud + hinda käike*/
  maximize(board) {

    let bestMove;
    let bestValue = -Infinity;
    const moves = this.boardFreeSpots(board);

    if(this.checkForWinner()) return -1;    
    if(!moves.length) return 0;
    
    for(let i=0; i < moves.length; i++) {
      board[moves[i]] = this.state.AI_SYMBOL;     

      let hValue = this.minimize(board);    
     
      if(Array.isArray(hValue)) {        
        hValue = hValue[0];        
      }

      if(hValue > bestValue) {
        bestMove = moves[i];
        bestValue = hValue;        
      }

      board[moves[i]] = "";
    }

    return [bestValue, bestMove];
  }

  /* Proovi läbi kõik mängija käigud + hinda käike */
  minimize(board) {
    
    let bestMove;
    let bestValue = Infinity;
    const moves = this.boardFreeSpots(board);
    

    if(this.checkForWinner()) return 1;
    if(!moves.length) return 0;

    const depth =  (this.state.aiDifficulty === 1) ? moves.length : 1;

    for(let i=0; i < depth; i++) {
      
      board[moves[i]] = this.state.P1_SYMBOL;

      let hValue = this.maximize(board);

      if(Array.isArray(hValue)) {
        hValue = hValue[0];
      }

      if(hValue < bestValue) {
        bestMove = moves[i];
        bestValue = hValue;
      }

      board[moves[i]] = "";
    }
   
    return [bestValue, bestMove];
  }

  /* Arvuti käik */
  aiMove = board => {    
    const bestMove = this.maximize(board);      
    this.state.board[bestMove[1]] = this.state.AI_SYMBOL;    
  }

  /* Mitu käiku veel teha saab, (see vist hetkel polegi kasutuses) */
  movesLeft(board, value = "") {
    const count = board.filter(i => i === value).length;
    this.setState({ movesLeft: count });   

    if( !count ) {
      this.setState({ gameOver: true, tieCount: this.state.tieCount+1 })
    }
  }  

  /* Taasta laud a.k.a uus mäng */
  resetBoard() {
    this.setState({ 
      board: ["","","","","","","","",""],
      winningCombo: [],
      currentTurn: "X",      
      movesLeft: 9,
      winner: "",
      gameOver: false,
    })

    if( !this.state.audioMuted ) fight.play();
  }

  /* Kontrolli võitjaid vastavalt kombinatsioonidele */
  checkForWinner() {
    // Võimalikud võidu kombinatsioonid + sisestatud sümbolite lugemine
    var combos = [[0,3,6], [1,4,7], [2,5,8], [0,1,2], [3,4,5], [6,7,8], [0,4,8], [2,4,6]];
    var symbols = this.state.board.map(function(square) {
      return square
    })
        
    return combos.find(function(combo) {      
      if(symbols[combo[0]] === symbols[combo[1]] && symbols[combo[1]] === symbols[combo[2]]) {        
        return symbols[combo[0]]
      } else {
        return false
      }
    })
  }

  /* Klikk square'il, tee midagi */
  handleClick(index) { 
    
    const board = this.state.board;    

    if((this.state.board[index] === "" || this.state.board[index] === null) && !this.state.gameOver) {
      
      // Täida square mängija sümboliga ja ai käib kohe vastu
      this.state.board[index] = this.state.P1_SYMBOL;      
      this.aiMove(board);      
         
      // Salvesta laua sisu + muuda sümbolit
      this.setState({        
        board: this.state.board        
      });
           
      var checkResult = this.checkForWinner();
      // kontrolli kas keegi on võitnud
      if(checkResult) {
        this.setState({ winningCombo: checkResult })
        // mängija võitis
        if(this.state.board[checkResult[0]] === this.state.P1_SYMBOL) {        
          this.setState({
            gameOver: true,
            winner: 'Player',
            playerScore: this.state.playerScore+1
          })
          if( !this.state.audioMuted ) quan_chi_wins.play();
        // arvuti võitis
        } else if(this.state.board[checkResult[0]] === this.state.AI_SYMBOL) {
          this.setState({
            gameOver: true,
            winner: 'AI',
            aiScore: this.state.aiScore+1
          })
          if( !this.state.audioMuted ) shinak_wins.play();
        }               
      // rohkem käike teha ei saa, viik        
      } else if(!this.boardFreeSpots(board).length) {      
        this.setState({tieCount: this.state.tieCount+1});
        if( !this.state.audioMuted ) tie.play();
      }
    // viik, kõik väljad on täidetud, klikk laual puhastab laua ja alustab uut mängu
    } else if(!this.boardFreeSpots(board).length) {            
      this.resetBoard();
    } else if(this.state.gameOver) {
      this.resetBoard();
    }
       
  }
  
  render() {
    return (            
      <div className="wrapper">
        <Nav changeAi={this.updateAiDifficulty.bind(this)} aiDiff={this.state.aiDifficulty} toggleMute={this.toggleMute.bind(this)} muteState={this.state.audioMuted}></Nav>
        <div className="content boardContainer">
          <div className="board">                                          
            {this.state.board.map((square, index) => {                                        
              return <div className={`square noselect ${ square === 'X' ? 'squareX' : (square === 'O') ? 'squareO' : '' } ${ ( this.state.winningCombo.includes(index) ) ? 'win-color' : ( this.state.gameOver ) ? 'faded' : '' }`} onClick={() => this.handleClick(index)} key={index} data-square-id={index}>{square}</div>
            })}                   
          </div>
        </div>
        <Footer playerScore={this.state.playerScore} aiScore={this.state.aiScore} winner={ this.state.winner } tieCount={ this.state.tieCount } gameOver={ this.state.gameOver } />
      </div>      
    );
    
  }
}

export default App;
